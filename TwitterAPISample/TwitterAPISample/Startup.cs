﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TwitterAPISample.Startup))]
namespace TwitterAPISample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
