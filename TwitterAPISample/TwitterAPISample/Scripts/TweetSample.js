$(function () {
    $("#search_text").focus().tweetsearch({
        page: 1,
        failureFunction: showFailure,
        callbackFunction: searchCallback
    });

    $(document).on("click", ".likes", function () {
        var likeButton = $(this);
        if (likeButton.hasClass("liked")) {
            return false;
        }
        var id = likeButton.closest(".tweet").data("id");
        $.post("/tweets/like", { id: id, __RequestVerificationToken: $("input[name=__RequestVerificationToken]").val() }, function (data) {
            if (data.success) {
                likeButton.fadeOut(500, function () {
                    likeButton.html(data.count);
                    likeButton.addClass("liked");
                    likeButton.fadeIn(500);
                });
            } else {
                showFailure();
            }

        });
        return false;
    });
});

function searchCallback(data) {
    $(".loader").hide();
    if (data.success) {
        $(".tweets_holder").html(data.html);
        $(".tweets_holder").fadeIn(1000);
    } else {
        showFailure();
    }
}

function showFailure() {
    window.scrollTo(0, 0);
    $(".tweets_holder").html("");
    $(".message").html("Web request failed!");
    $(".message").slideDown(700);
    $(".message").delay(4000).slideUp(700);
}