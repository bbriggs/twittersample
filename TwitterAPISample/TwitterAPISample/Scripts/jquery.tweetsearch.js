(function ($) {

    $.fn.tweetsearch = function (options) {

        var options = $.extend({
            page: 1,
            buttonElement: $("#search_button"),
            failureFunction: null,
            searchUrl: "/tweets/search",
            callbackFunction: null
        }, options);

        var searchElement = this;

        searchElement.keypress(function (e) {
            if (e.keyCode == 13) {
                options.buttonElement.click();
            }
        });

        options.buttonElement.on("click", function () {
            var query = searchElement.val();
            if (query.length > 0) {
                getSearchResults(query, options.page, options.searchUrl, options.callbackFunction);
            }
            return false;
        });

        $(document).on("keypress", "#page_nav_text", function (e) {
            if (e.keyCode == 13) {
                var page = parseInt($("#page_nav_text").val());
                var currentPage = parseInt($(".pagination").data("current-page"));
                if (page > 0 && page != currentPage) {
                    getSearchResults(searchElement.val(), page, options.searchUrl, options.callbackFunction);
                }
            }
        });

        $(document).on("click", ".page_link", function () {
            var page = $(this).data("page");
            getSearchResults(searchElement.val(), page, options.searchUrl, options.callbackFunction);
            return false;
        });

        return this;
    }

    function getSearchResults(query, page, searchUrl, callback) {
        $(".message").html("");
        $(".tweets_holder").fadeOut(1000, function () {
            $(".loader").show();
            $.get(searchUrl, { query: query, page: page }, function (data) {
                if (callback != null) {
                    callback(data);
                }
            });
        });
    }

}(jQuery));