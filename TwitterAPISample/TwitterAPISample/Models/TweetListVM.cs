﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using TwitterAPISample.TwitterModels;

namespace TwitterAPISample.Models
{
    /// <summary>
    /// Tweet List ViewModel - Holds the list of tweets and other data needed in the view
    /// </summary>
    public class TweetListVM : IValidation
    {
        #region Properties
        /// <summary>
        /// Number of pages returned by the search
        /// </summary>
        public int Pages { get; set; }

        /// <summary>
        /// The list of tweets returned by the search query
        /// </summary>
        public List<Tweet> Tweets { get; set; } = new List<Tweet>();

        /// <summary>
        /// Number of tweets returned by the search query
        /// </summary>
        public int TweetCount { get; set; }

        /// <summary>
        /// Current page the user is viewing
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// The query string being searched on
        /// </summary>
        public string Query { get; set; }
        #endregion

        #region IValidation
        /// <summary>
        /// Collection of errors
        /// </summary>
        public NameValueCollection Errors { get; private set; } = new NameValueCollection();

        /// <summary>
        /// Validates the view model
        /// </summary>
        public void Validate()
        {
            if (string.IsNullOrEmpty(Query))
                Errors.Add("Query", "Please enter at least one hashtag to search with");
        }
        #endregion
    }
}