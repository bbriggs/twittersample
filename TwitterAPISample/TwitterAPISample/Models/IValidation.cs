﻿using System;
using System.Collections.Specialized;

namespace TwitterAPISample.Models
{
    /// <summary>
    /// interface for validating a view model
    /// </summary>
    public interface IValidation
    {
        /// <summary>
        /// Validation method - does the validation
        /// </summary>
        void Validate();

        /// <summary>
        /// Collection of errors
        /// </summary>
        NameValueCollection Errors { get; }
    }
}