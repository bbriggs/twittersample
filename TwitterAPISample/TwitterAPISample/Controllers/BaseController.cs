﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TwitterAPISample.Business;
using TwitterAPISample.Models;
using TwitterAPISample.TwitterModels;

namespace TwitterAPISample.Controllers
{
    public class BaseController : Controller
    {
        protected string RenderPartialToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        protected void ValidateModel(IValidation model)
        {            model.Validate();
            foreach (var key in model.Errors.AllKeys)
                ModelState.AddModelError(key, model.Errors[key]);
        }

        //this would be refactored out if there was more than one location it was being used, keeping it simple since it is only used here
        protected string BuildOAuthSignature(string requestType, string url, OAuthCredentials credentials, List<TwitterParameter> parameters)
        {
            var hasher = new HashFactory(EscapeDataString(credentials.PrivateKey), EscapeDataString(credentials.PrivateToken)).GetHasher();
            if (hasher == null)
                return string.Empty;
            var baseString = EscapeDataString(string.Join("&", parameters.Select(p => string.Format("{0}={1}", EscapeDataString(p.Name), EscapeDataString(p.Value)))));

            baseString = string.Format("{0}&{1}&{2}", requestType, EscapeDataString(url), baseString);

            return Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));
        }

        //this would be refactored out if there was more than one location it was being used, keeping it simple since it is only used here
        protected string BuildOAuthAuthorization(string publicKey, string publicToken, List<TwitterParameter> parameters)
        {
            return "OAuth " + string.Join(",", parameters.Where(p => p.ParamType == ParameterType.OAuth).Select(p => string.Format(@"{0}=""{1}""", p.Name, p.Value)));
        }

        protected string EscapeDataString(string content)
        {
            return Uri.EscapeDataString(content);
        }
    }
}