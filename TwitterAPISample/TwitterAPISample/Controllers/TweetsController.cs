﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TwitterAPISample.Business;
using TwitterAPISample.Models;
using TwitterAPISample.TwitterModels;

namespace TwitterAPISample.Controllers
{
    public class TweetsController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Search(string query, int page)
        {
            List<Tweet> tweets;
            var success = false;
            var credentials = GetCredentials();

            var viewModel = new TweetListVM()
            {
                Query = query.Replace("#", "")
            };

            var parameters = new ParameterBuilder().GetSearchParameters(viewModel.Query, credentials.PublicKey, credentials.PublicToken);
            parameters.Sort();

            ValidateModel(viewModel); 

            if (ModelState.IsValid)
            {
                tweets = GetTweetsFromAPIRequest(viewModel.Query, credentials, parameters);
                success = tweets != null;
                if (success)
                {
                    tweets.Sort();
                    viewModel.Pages = (int)Math.Ceiling((double)tweets.Count / 10);
                    viewModel.Tweets = tweets.Skip((page - 1) * 10).Take(10).ToList();
                    viewModel.TweetCount = tweets.Count;
                    viewModel.CurrentPage = page;
                }
            }

            if (Request.IsAjaxRequest())
            {
                return Json(new { success = success, html = RenderPartialToString("_tweetList", viewModel) }, JsonRequestBehavior.AllowGet);
            }

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Like(string id)
        {
            var credentials = GetCredentials();
            var success = false;
            var parameters = new ParameterBuilder().GetOAuthParameters(credentials.PublicKey, credentials.PublicToken);
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.Request, Name = "id", Value = id });
            parameters.Sort();

            var likedTweet = LikeTweet(id, credentials, parameters);
            success = likedTweet != null;

            if (Request.IsAjaxRequest())
            {
                return Json(new { success = success, count = likedTweet != null ? likedTweet.LikeCount : -1 });
            }

            return null;
        }

        private List<Tweet> GetTweetsFromAPIRequest(string query, OAuthCredentials credentials, List<TwitterParameter> parameters)
        {
            var baseUrl = UrlType.Search.GetUrl();
            var urlString = new UrlFactory(baseUrl, parameters.Where(p => p.ParamType == ParameterType.Request).ToList()).BuildQueryUrl();

            var oAuthSignature = BuildOAuthSignature("GET", baseUrl, credentials, parameters);
            parameters.Add(new TwitterParameter { ParamType = ParameterType.OAuth, Name = "oauth_signature", Value = EscapeDataString(oAuthSignature) });

            var apiRequest = (HttpWebRequest)WebRequest.Create(urlString);
            apiRequest.Headers.Add("Authorization", BuildOAuthAuthorization(credentials.PublicKey, credentials.PublicToken, parameters));
            apiRequest.Method = "GET";
            apiRequest.Host = "api.twitter.com";

            List<Tweet> tweets = new List<Tweet>();
            try
            {
                using (WebResponse apiResponse = apiRequest.GetResponse())
                {
                    using (var reader = new StreamReader(apiResponse.GetResponseStream()))
                    {
                        var responseContent = reader.ReadToEnd();

                        JToken allTweets = JObject.Parse(responseContent);

                        var statuses = allTweets.SelectToken("statuses");
                        tweets = statuses.ToObject<List<Tweet>>();
                    }
                }
            }
            catch //failed request - handling this generic with any failed request for simplicity
            {
                return null;
            }

            return tweets;
        }

        private OAuthCredentials GetCredentials()
        {
            return new OAuthCredentials()
            {
                PublicKey = ConfigurationManager.AppSettings["PublicKey"],
                PrivateKey = ConfigurationManager.AppSettings["PrivateKey"],
                PublicToken = ConfigurationManager.AppSettings["PublicToken"],
                PrivateToken = ConfigurationManager.AppSettings["PrivateToken"]
            };
        }

        private Tweet LikeTweet(string id, OAuthCredentials credentials, List<TwitterParameter> parameters)
        {
            var baseUrl = UrlType.Like.GetUrl();
            var likeUrl = new UrlFactory(baseUrl, parameters.Where(p => p.ParamType == ParameterType.Request).ToList()).BuildLikeUrl();

            var oAuthSignature = BuildOAuthSignature("POST", baseUrl, credentials, parameters);
            parameters.Add(new TwitterParameter { ParamType = ParameterType.OAuth, Name = "oauth_signature", Value = EscapeDataString(oAuthSignature) });

            var apiRequest = (HttpWebRequest)WebRequest.Create(likeUrl);
            apiRequest.Headers.Add("Authorization", BuildOAuthAuthorization(credentials.PublicKey, credentials.PublicToken, parameters));
            apiRequest.Method = "POST";
            apiRequest.Host = "api.twitter.com";

            Tweet tweet;
            try
            {
                using (WebResponse apiResponse = apiRequest.GetResponse())
                {
                    using (var reader = new StreamReader(apiResponse.GetResponseStream()))
                    {
                        var responseContent = reader.ReadToEnd();

                        JToken likedTweet = JObject.Parse(responseContent);

                        tweet = likedTweet.ToObject<Tweet>();
                    }
                }
            }
            catch //failed request - handling this generic with any failed request for simplicity
            {
                return null;
            }

            return tweet;
        }
    }
}