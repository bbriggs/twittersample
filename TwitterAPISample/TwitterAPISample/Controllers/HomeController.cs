﻿using System;
using System.Web.Mvc;

namespace TwitterAPISample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "This application simply allows you to search for tweets based on a #hashtag by using the twitter API.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Bryson Briggs - briggs.fervent@gmail.com";

            return View();
        }
    }
}