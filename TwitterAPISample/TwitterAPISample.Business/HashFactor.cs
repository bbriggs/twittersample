﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TwitterAPISample.Business
{
    public class HashFactory
    {
        private string PrivateKey { get; set; }
        private string PrivateToken { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="privateKey">app private key</param>
        /// <param name="privateToken">user private token</param>
        public HashFactory(string privateKey, string privateToken)
        {
            PrivateKey = privateKey;
            PrivateToken = privateToken;
        }

        /// <summary>
        /// Gets the hash 
        /// </summary>
        /// <returns></returns>
        public HMACSHA1 GetHasher()
        {
            if (string.IsNullOrEmpty(PrivateKey) || string.IsNullOrEmpty(PrivateToken))
                return null;

            return new HMACSHA1(new ASCIIEncoding().GetBytes(string.Format("{0}&{1}", PrivateKey, PrivateToken)));
        }
    }
}
