﻿using System;
using System.Linq;
using System.Collections.Generic;
using TwitterAPISample.TwitterModels;

namespace TwitterAPISample.Business
{
    public class ParameterBuilder
    {
        /// <summary>
        /// Builds OAuth parameters
        /// </summary>
        /// <param name="publicKey">public key for the app</param>
        /// <param name="publicToken">public token for the user</param>
        /// <returns></returns>
        public List<TwitterParameter> GetOAuthParameters(string publicKey, string publicToken)
        {
            var parameters = new List<TwitterParameter>();
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.OAuth, Name = "oauth_consumer_key", Value = publicKey });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.OAuth, Name = "oauth_signature_method", Value = "HMAC-SHA1" });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.OAuth, Name = "oauth_timestamp", Value = ((int)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds)).ToString() });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.OAuth, Name = "oauth_nonce", Value = Guid.NewGuid().ToString() });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.OAuth, Name = "oauth_version", Value = "1.0" });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.OAuth, Name = "oauth_token", Value = publicToken });
            return parameters;
        }


        /// <summary>
        /// Gets list of search parameters
        /// </summary>
        /// <param name="query">query the user is searching for</param>
        /// <param name="publicKey">public key for the app</param>
        /// <param name="publicToken">public token for the user</param>
        /// <returns></returns>
        public List<TwitterParameter> GetSearchParameters(string query, string publicKey, string publicToken)
        {
            var parameters = GetOAuthParameters(publicKey, publicToken);
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.Request, Name = "q", Value = string.Join(" ", query.Split(null).Select(v => "#" + v)) });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.Request, Name = "count", Value = "50" });
            parameters.Add(new TwitterParameter() { ParamType = ParameterType.Request, Name = "result_type", Value = "popular" });
            return parameters;
        }
    }
}
