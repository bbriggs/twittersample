﻿using System;
using System.Collections.Generic;
using System.Linq;
using TwitterAPISample.TwitterModels;

namespace TwitterAPISample.Business
{
    public class UrlFactory
    {
        private string BaseUrl { get; set; }

        private List<TwitterParameter> UrlParameters { get; set; }

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="baseUrl">Base Url of the request</param>
        /// <param name="urlParams">Parameters the request uses</param>
        public UrlFactory(string baseUrl, List<TwitterParameter> urlParams)
        {
            BaseUrl = baseUrl;
            UrlParameters = urlParams;
        }

        /// <summary>
        /// Builds a query/search url
        /// </summary>
        /// <returns></returns>
        public string BuildQueryUrl()
        {
            var queryParam = UrlParameters.Find(p => p.Name == "q");
            if (queryParam == null)
                return string.Empty;

            string parametersString = "?";

            parametersString += string.Join("&", UrlParameters.Select(p =>
            {
                if (p.Name == "q")
                {
                    var hashTags = p.Value.Split(' ');

                    var queryValue = string.Join("%20", hashTags.Select(h => string.Format("%23{0}", h.Replace("#", ""))));
                    return string.Format("{0}={1}", p.Name, queryValue);
                }
                else
                {
                    return string.Format("{0}={1}", p.Name, p.Value);
                }
            }));

            return BaseUrl + parametersString;
        }

        /// <summary>
        /// Builds a like url
        /// </summary>
        /// <returns></returns>
        public string BuildLikeUrl()
        {
            var idParam = UrlParameters.Find(p => p.Name == "id");
            if (idParam == null)
                return string.Empty;

            var idString = "?id=" + idParam.Value;
            return BaseUrl + idString;
        }
    }
}
