﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TwitterAPISample.TwitterModels
{
    /// <summary>
    /// Class to convert twitter's date time format to a usable one
    /// </summary>
    public class TwitterDateTimeConverter : DateTimeConverterBase
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public TwitterDateTimeConverter() { }

        /// <summary>
        /// Reads the Json
        /// </summary>
        /// <param name="reader">JsonReader to read from</param>
        /// <param name="objectType">Type type of object</param>
        /// <param name="existingValue">object existing value of object being read</param>
        /// <param name="serializer">JsonSerializer serializer</param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return DateTime.ParseExact(reader.Value.ToString(), "ddd MMM dd HH:mm:ss +ffff yyyy", new System.Globalization.CultureInfo("en-US"));
        }

        /// <summary>
        /// Writes the JSon - Not implemented
        /// </summary>
        /// <param name="writer">JsonWriter to write to</param>
        /// <param name="value">object value to write</param>
        /// <param name="serializer">JsonSerializer serializer</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}