﻿using System;

namespace TwitterAPISample.TwitterModels
{
    /// <summary>
    /// Class that represents a twitter parameter for authorizing or querying
    /// </summary>
    public class TwitterParameter : IComparable<TwitterParameter>
    {
        /// <summary>
        /// Type of parameter this represents
        /// </summary>
        public ParameterType ParamType { get; set; }

        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Value of the parameter
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Sorting logic, Compares by name
        /// </summary>
        /// <param name="other">The other item to compare to</param>
        /// <returns></returns>
        public int CompareTo(TwitterParameter other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }

    /// <summary>
    /// Enumeration of parameter types for simpler manipulation
    /// </summary>
    public enum ParameterType
    {
        /// <summary>
        /// Not set/not used
        /// </summary>
        NotSet = 0,

        /// <summary>
        /// OAuth parameter - used in Authorization header
        /// </summary>
        OAuth = 1,

        /// <summary>
        /// Request parameter - used in the actual request (in the search or like)
        /// </summary>
        Request = 2
    }
}
