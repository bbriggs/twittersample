﻿using System;
using Newtonsoft.Json;

namespace TwitterAPISample.TwitterModels
{
    /// <summary>
    /// Class to represent a tweet
    /// </summary>
    public class Tweet : IComparable<Tweet>
    {

        #region Properties
        /// <summary>
        /// ID of the tweet
        /// </summary>
        [JsonProperty("id_str")]
        public string ID { get; set; }

        /// <summary>
        /// Content or text of the tweet
        /// </summary>
        [JsonProperty("text")]
        public string TweetContent { get; set; }

        /// <summary>
        /// The user who posted the tweet
        /// </summary>
        [JsonProperty("user")]
        public TwitterUser Author { get; set; }

        /// <summary>
        /// Time the tweet was posted
        /// </summary>
        [JsonProperty("created_at")]
        [JsonConverter(typeof(TwitterDateTimeConverter))]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Number of retweets for the tweet
        /// </summary>
        [JsonProperty("retweet_count")]
        public int RetweetCount { get; set; }

        /// <summary>
        /// Number of likes/favorites for the tweet
        /// </summary>
        [JsonProperty("favorite_count")]
        public int LikeCount { get; set; }
        #endregion

        #region IComparable
        public int CompareTo(Tweet other)
        {
            return -(this.RetweetCount + this.LikeCount).CompareTo(other.RetweetCount + other.LikeCount);
        }

        #endregion
    }
}