﻿using System;
using Newtonsoft.Json;

namespace TwitterAPISample.TwitterModels
{
    /// <summary>
    /// Class representing a twitter user
    /// </summary>
    public class TwitterUser
    {
        /// <summary>
        /// Full Name of the user
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// URL to the user's profile picture
        /// </summary>
        [JsonProperty("profile_image_url")]
        public string ProfilePicUrl { get; set; }

        /// <summary>
        /// Screen name of the user - @name
        /// </summary>
        [JsonProperty("screen_name")]
        public string ScreenName { get; set; }
    }
}