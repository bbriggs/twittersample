﻿using System;

namespace TwitterAPISample.TwitterModels
{
    public class OAuthCredentials
    {
        public string PublicKey { get; set; }

        public string PrivateKey { get; set; }

        public string PublicToken { get; set; }

        public string PrivateToken { get; set; }
    }
}
