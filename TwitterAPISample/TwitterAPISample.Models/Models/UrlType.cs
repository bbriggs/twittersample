﻿using System;

namespace TwitterAPISample.TwitterModels
{
    /// <summary>
    /// Enumeration to determine the URL of the API call
    /// </summary>
    public enum UrlType
    {
        /// <summary>
        /// Not set/not used
        /// </summary>
        NotSet = 0,

        /// <summary>
        /// Search tweets
        /// </summary>
        Search = 1,

        /// <summary>
        /// Like a tweet
        /// </summary>
        Like = 2,
    }

    /// <summary>
    /// Extension methods for UrlType
    /// </summary>
    public static class UrlTypeExtensions
    {
        /// <summary>
        /// Gets the URL based on the url type
        /// </summary>
        /// <param name="type">UrlType type of URL/API call</param>
        /// <returns></returns>
        public static string GetUrl(this UrlType type)
        {
            string url = "";

            switch (type)
            {
                case UrlType.Search:
                    url = "https://api.twitter.com/1.1/search/tweets.json";
                    break;
                case UrlType.Like:
                    url = "https://api.twitter.com/1.1/favorites/create.json";
                    break;
            }
            return url;
        }
    }
}
