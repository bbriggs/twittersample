Simple sample twitter API implementation.

Keys & Tokens must be entered into the web config.

Keys = app key | Tokens = user token


1. Allows user to search for hashtags(s)
2. Searches popular tweets through twitter API by provided hashtags and sorts them by popularity (likes and retweets)
3. Displays relevant tweet information in pages (10 per page)
4. Allows user to 'like' a tweet by clicking a heart